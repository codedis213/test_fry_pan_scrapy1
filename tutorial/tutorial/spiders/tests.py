from bs4 import BeautifulSoup
import requests
import re
from lxml import html

link = "http://www.korasoi.com/blog/2016/01/vegan-tandoori-tofu-tikka-masala"

r = requests.get(link)
page = r.content
r.close()

soup = BeautifulSoup(page, "html.parser")
p = soup.find("p", text=re.compile("Ingredients"))

p_list = []

flag = True


while flag:
	p = p.find_next_sibling("p")

	if p:
		p_txt = p.get_text().encode("ascii", "ignore").strip()

		if  p_txt in ["Method", "Method:"]:
			flag = False
		else:
			print p_txt
			p_list.append("%s" %(p))
	else:
		flag = False
